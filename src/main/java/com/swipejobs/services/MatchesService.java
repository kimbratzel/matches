package com.swipejobs.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.swipejobs.models.Job;
import com.swipejobs.models.Worker;

@Service
public interface MatchesService {
    /**
     * Matches a worker to a list of jobs
     * 
     * @param worker
     * @param jobs
     * @param limit
     * @return A list of jobs that match the given worker, of maximum size limit
     */
    List<Job> findMatchesForWorker(final Worker worker, final List<Job> jobs, final Integer limit);
}
