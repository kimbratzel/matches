package com.swipejobs.services;

import java.io.File;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;

@Service
public interface DataService {

    /**
     * Deserialize JSON into Models from a URL
     * Assumes no errors, well-formed etc
     * @return 
     */
    @SuppressWarnings("rawtypes")
    <T> T fetchData(URL url, TypeReference valueTypeRef);

    /**
     * Deserialize JSON into Models from a JSON File
     *  - Faster than URL, used for testing, etc
     * Assumes no errors, well-formed etc
     * @return 
     */
    @SuppressWarnings("rawtypes")
    <T> T fetchData(File src, TypeReference valueTypeRef);
}
