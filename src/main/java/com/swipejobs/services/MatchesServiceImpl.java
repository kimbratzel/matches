package com.swipejobs.services;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.swipejobs.helpers.DistanceHelper;
import com.swipejobs.models.Job;
import com.swipejobs.models.Worker;

@Service
public class MatchesServiceImpl implements MatchesService{

    /**
     * Match Worker to Jobs
     */
    @Override
    public List<Job> findMatchesForWorker(final Worker worker, final List<Job> jobs, final Integer limit) {
        
        //Filter out jobs that don't match the worker (high quality matches only)
        List<Job> matches = jobs.stream().filter(job -> isMatchStrict(worker, job)).collect(Collectors.toList());
        
        //If we couldn't find many high quality matches let's look for low quality matches
        if(matches.size() < limit) {
            matches = jobs.stream().filter(job -> isMatchLiberal(worker, job)).collect(Collectors.toList());
        }
        //Return at most limit number of matches
        return matches.subList(0, Math.min(matches.size(), limit));
    }

    /**
     * Match worker to job in a strict manner
     * (Less likely to return a match but matches are possibly higher quality)
     * @param worker
     * @param job
     * @return
     */
    protected Boolean isMatchStrict(Worker worker, Job job) {
        return workersRequiredMatch(worker, job) &&
               availabilityMatch(worker, job) &&
               distanceMatch(worker, job) &&
               skillsMatch(worker, job) &&
               certificatesMatch(worker, job) &&
               licenseMatch(worker, job);
      }

    /**
     * Match worker to job in liberal manner
     * (More likely to return a match but matches are generally lower quality)
     * @param worker
     * @param job
     * @return
     */
    protected Boolean isMatchLiberal(Worker worker, Job job) {
        return workersRequiredMatch(worker, job) &&
               availabilityMatch(worker, job) &&
               distanceMatch(worker, job) &&
               certificatesMatch(worker, job) &&
               licenseMatch(worker, job);
      }

    /**
     * Returns true if a worker has all required certificates for a job
     * @param worker
     * @param job
     * @return
     */
    private Boolean certificatesMatch(Worker worker, Job job) {
        return worker.getCertificates().containsAll(job.getRequiredCertificates());
    }

    /**
     * Returns true if job title matches a one of a worker's skills
     * @param worker
     * @param job
     * @return
     */
    private Boolean skillsMatch(Worker worker, Job job) {
        return worker.getSkills().contains(job.getJobTitle());
    }

    /**
     * Returns true if the distance between a worker and job is within the worker's maxJobDistance
     * @param worker
     * @param job
     * @return
     */
    private Boolean distanceMatch(Worker worker, Job job) {
        return DistanceHelper.withinRange(worker, job);
    }

    /**
     * Returns true if a license is not required or it is and the worker has one
     * @param worker
     * @param job
     * @return
     */
    private Boolean licenseMatch(Worker worker, Job job) {
        if (!job.isDriverLicenseRequired()) {
            return true;
        }
        else if (job.isDriverLicenseRequired() && worker.isHasDriversLicense()) {
            return true;
        }
        return false;
    }

    /**
     * Returns true if a job requires at least 1 worker
     * @param worker
     * @param job
     * @return
     */
    private Boolean workersRequiredMatch(Worker worker, Job job) {
        return job.getWorkersRequired() > 0;
    }

    /**
     * Returns true if a worker is available on the day of the week the job is offered
     * @param worker
     * @param job
     * @return
     */
    private Boolean availabilityMatch(Worker worker, Job job) {
        int jobStartDayOfWeek = job.getStartDate().getDayOfWeek().getValue();
        return worker.getAvailability().stream().anyMatch(a -> a != null && jobStartDayOfWeek == a.getDayIndex());
    }

}
