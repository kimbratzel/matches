package com.swipejobs.services;

import java.io.File;
import java.net.URL;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class DataServiceImpl implements DataService {

    /**
     * Deserialize JSON into Models from a JSON URL
     * Assumes no errors, well-formed etc
     * @return 
     */
    @SuppressWarnings("rawtypes")
    public <T> T fetchData(URL url, TypeReference valueTypeRef) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(url, valueTypeRef);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Deserialize JSON into Models from a JSON File
     *  - Faster than URL, used for testing, etc
     * Assumes no errors, well-formed etc
     * @return 
     */
    @SuppressWarnings("rawtypes")
    public <T> T fetchData(File src, TypeReference valueTypeRef) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(src, valueTypeRef);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
