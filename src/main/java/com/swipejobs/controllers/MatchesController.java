package com.swipejobs.controllers;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.type.TypeReference;
import com.swipejobs.models.Job;
import com.swipejobs.models.Worker;
import com.swipejobs.services.DataService;
import com.swipejobs.services.MatchesService;

@Controller
public class MatchesController {

    //Config
    @Value("${matches.apiEndpoint}")
    private String apiEndpoint;
    @Value("${matches.limit}")
    private int limit;

    //Inject services
    @Autowired
    private MatchesService matchesService;
    @Autowired
    private DataService dataService;

    Logger logger = Logger.getLogger(MatchesController.class);
    
    /**
     * Matches worker to jobs given a workerId.
     * 
     * @param workerId
     * @return JSON List of Jobs that match the Worker
     */
    @RequestMapping(value = "/api/matches", method = RequestMethod.GET)
    @ResponseBody
    public List<Job> matches(@RequestParam("workerId") Integer workerId) {
        logger.info("Find Matches for Worker " + workerId);
        
        List<Worker> workers = new ArrayList<Worker>();
        List<Job> jobs = new ArrayList<Job>();
        //Fetch jobs/workers from API (assumed to be available and error free)
        try {
            workers = dataService.fetchData(new URL(apiEndpoint + "workers"), new TypeReference<List<Worker>>(){});
            jobs = dataService.fetchData(new URL(apiEndpoint + "jobs"), new TypeReference<List<Job>>(){});
        } catch (Exception e) {
            e.printStackTrace();
        }

        //Assume userId exists
        Worker worker = workers.stream().filter(w -> workerId == w.getUserId()).findFirst().orElse(null);
        
        List<Job> matches = matchesService.findMatchesForWorker(worker, jobs, limit);
        return matches;
    }
}
