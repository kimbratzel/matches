package com.swipejobs.models;

/**
 * Worker Model
 */
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public final class Worker {
    private double rating;
    private boolean isActive;
    private List<String> certificates;
    private List<String> skills;
    private JobSearchAddress jobSearchAddress;
    private String transportation;
    private boolean hasDriversLicense;
    private List<Availability> availability;
    private String phone;
    private String email;
    private Name name;
    private double age;
    private String guid;
    private double userId;

    @JsonCreator
    public Worker(@JsonProperty("rating") double rating, @JsonProperty("isActive") boolean isActive, @JsonProperty("certificates") List<String> certificates, @JsonProperty("skills") List<String> skills, @JsonProperty("jobSearchAddress") JobSearchAddress jobSearchAddress, @JsonProperty("transportation") String transportation, @JsonProperty("hasDriversLicense") boolean hasDriversLicense, @JsonProperty("availability") List<Availability> availability, @JsonProperty("phone") String phone, @JsonProperty("email") String email, @JsonProperty("name") Name name, @JsonProperty("age") double age, @JsonProperty("guid") String guid, @JsonProperty("userId") double userId){
        this.rating = rating;
        this.isActive = isActive;
        this.certificates = certificates;
        this.skills = skills;
        this.jobSearchAddress = jobSearchAddress;
        this.transportation = transportation;
        this.hasDriversLicense = hasDriversLicense;
        this.availability = availability;
        this.phone = phone;
        this.email = email;
        this.name = name;
        this.age = age;
        this.guid = guid;
        this.userId = userId;
    }

    public Worker() {}

    public double getRating() {
        return rating;
    }

    public Worker setRating(double rating) {
        this.rating = rating;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public Worker setActive(boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public List<String> getCertificates() {
        return certificates;
    }

    public Worker setCertificates(List<String> certificates) {
        this.certificates = certificates;
        return this;
    }

    public List<String> getSkills() {
        return skills;
    }

    public Worker setSkills(List<String> skills) {
        this.skills = skills;
        return this;
    }

    public JobSearchAddress getJobSearchAddress() {
        return jobSearchAddress;
    }

    public Worker setJobSearchAddress(JobSearchAddress jobSearchAddress) {
        this.jobSearchAddress = jobSearchAddress;
        return this;
    }

    public String getTransportation() {
        return transportation;
    }

    public Worker setTransportation(String transportation) {
        this.transportation = transportation;
        return this;
    }

    public boolean isHasDriversLicense() {
        return hasDriversLicense;
    }

    public Worker setHasDriversLicense(boolean hasDriversLicense) {
        this.hasDriversLicense = hasDriversLicense;
        return this;
    }

    public List<Availability> getAvailability() {
        return availability;
    }

    public Worker setAvailability(List<Availability> availability) {
        this.availability = availability;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public Worker setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Worker setEmail(String email) {
        this.email = email;
        return this;
    }

    public Name getName() {
        return name;
    }

    public Worker setName(Name name) {
        this.name = name;
        return this;
    }

    public double getAge() {
        return age;
    }

    public Worker setAge(double age) {
        this.age = age;
        return this;
    }

    public String getGuid() {
        return guid;
    }

    public Worker setGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public double getUserId() {
        return userId;
    }

    public Worker setUserId(double userId) {
        this.userId = userId;
        return this;
    }

    public static final class JobSearchAddress {
        private String unit;
        private double maxJobDistance;
        private double longitude;
        private double latitude;

        @JsonCreator
        public JobSearchAddress(@JsonProperty("unit") String unit, @JsonProperty("maxJobDistance") double maxJobDistance, @JsonProperty("longitude") double longitude, @JsonProperty("latitude") double latitude){
            this.unit = unit;
            this.maxJobDistance = maxJobDistance;
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }

        public double getMaxJobDistance() {
            return maxJobDistance;
        }

        public void setMaxJobDistance(double maxJobDistance) {
            this.maxJobDistance = maxJobDistance;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }

    public static final class Availability {
        private String title;
        private int dayIndex;

        @JsonCreator
        public Availability(@JsonProperty("title") String title, @JsonProperty("dayIndex") int dayIndex){
            this.title = title;
            this.dayIndex = dayIndex;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public int getDayIndex() {
            return dayIndex;
        }

        public void setDayIndex(int dayIndex) {
            this.dayIndex = dayIndex;
        }
    }

    public static final class Name {
        private String last;
        private String first;

        @JsonCreator
        public Name(@JsonProperty("last") String last, @JsonProperty("first") String first){
            this.last = last;
            this.first = first;
        }

        public String getLast() {
            return last;
        }

        public void setLast(String last) {
            this.last = last;
        }

        public String getFirst() {
            return first;
        }

        public void setFirst(String first) {
            this.first = first;
        }
    }
}