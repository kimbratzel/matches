package com.swipejobs.models;

import java.time.ZonedDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Job Model
 */
public final class Job {
    private boolean driverLicenseRequired;
    private List<String> requiredCertificates;
    private Location location;
    private String billRate;
    private double workersRequired;
    private ZonedDateTime startDate;
    private String about;
    private String jobTitle;
    private String company;
    private String guid;
    private long jobId;

    @JsonCreator
    public Job(@JsonProperty("driverLicenseRequired") boolean driverLicenseRequired, @JsonProperty("requiredCertificates") List<String> requiredCertificates, @JsonProperty("location") Location location, @JsonProperty("billRate") String billRate, @JsonProperty("workersRequired") double workersRequired, @JsonProperty("startDate") String startDate, @JsonProperty("about") String about, @JsonProperty("jobTitle") String jobTitle, @JsonProperty("company") String company, @JsonProperty("guid") String guid, @JsonProperty("jobId") long jobId){
        this.driverLicenseRequired = driverLicenseRequired;
        this.requiredCertificates = requiredCertificates;
        this.location = location;
        this.billRate = billRate;
        this.workersRequired = workersRequired;
        this.startDate = ZonedDateTime.parse(startDate);
        this.about = about;
        this.jobTitle = jobTitle;
        this.company = company;
        this.guid = guid;
        this.jobId = jobId;
    }

    public Job() {}

    public boolean isDriverLicenseRequired() {
        return driverLicenseRequired;
    }

    public Job setDriverLicenseRequired(boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
        return this;
    }

    public List<String> getRequiredCertificates() {
        return requiredCertificates;
    }

    public Job setRequiredCertificates(List<String> requiredCertificates) {
        this.requiredCertificates = requiredCertificates;
        return this;
    }

    public Location getLocation() {
        return location;
    }

    public Job setLocation(Location location) {
        this.location = location;
        return this;
    }

    public String getBillRate() {
        return billRate;
    }

    public Job setBillRate(String billRate) {
        this.billRate = billRate;
        return this;
    }

    public double getWorkersRequired() {
        return workersRequired;
    }

    public Job setWorkersRequired(double workersRequired) {
        this.workersRequired = workersRequired;
        return this;
    }

    public ZonedDateTime getStartDate() {
        return startDate;
    }

    public Job setStartDate(ZonedDateTime startDate) {
        this.startDate = startDate;
        return this;
    }

    public String getAbout() {
        return about;
    }

    public Job setAbout(String about) {
        this.about = about;
        return this;
    }

    public String getJobTitle() {
        return jobTitle;
    }

    public Job setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public Job setCompany(String company) {
        this.company = company;
        return this;
    }

    public String getGuid() {
        return guid;
    }

    public Job setGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public long getJobId() {
        return jobId;
    }

    public Job setJobId(long jobId) {
        this.jobId = jobId;
        return this;
    }

    public static final class Location {
        private double longitude;
        private double latitude;

        @JsonCreator
        public Location(@JsonProperty("longitude") double longitude, @JsonProperty("latitude") double latitude){
            this.longitude = longitude;
            this.latitude = latitude;
        }

        public double getLongitude() {
            return longitude;
        }

        public void setLongitude(double longitude) {
            this.longitude = longitude;
        }

        public double getLatitude() {
            return latitude;
        }

        public void setLatitude(double latitude) {
            this.latitude = latitude;
        }
    }
}