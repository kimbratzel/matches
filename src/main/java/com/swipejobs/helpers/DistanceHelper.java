package com.swipejobs.helpers;

import com.swipejobs.models.Job;
import com.swipejobs.models.Worker;

public class DistanceHelper {

    //Don't Instantiate
    private DistanceHelper() {}

    /**
     * Returns True is a job is geographically within a valid range of a worker.
     * Valid range is defined by the worker's maxJobSearchDistance
     * @param worker
     * @param job
     * @return true if a job is within valid range of worker
     */
    public static Boolean withinRange(Worker worker, Job job) {
        Double distanceBetween = distanceBetweenTwoPoints(
                worker.getJobSearchAddress().getLatitude(),
                worker.getJobSearchAddress().getLongitude(),
                job.getLocation().getLatitude(),
                job.getLocation().getLongitude(),
                worker.getJobSearchAddress().getUnit());
        return distanceBetween <= worker.getJobSearchAddress().getMaxJobDistance();
    }
    /**
     * Calculates the distance between two points (given the latitude/longitude of those points)
     * Shamelessly stolen from: http://www.geodatasource.com/developers/java
     * @param lat1
     * @param lon1
     * @param lat2
     * @param lon2
     * @param unit
     * @return Distance between two points (in Miles by default)
     */
    public static Double distanceBetweenTwoPoints(Double lat1, Double lon1, Double lat2, Double lon2, String unit) {
        Double theta = lon1 - lon2;
        Double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        //Convert to KM if required
        if ("km".equalsIgnoreCase(unit)) {
            dist = dist * 1.609344;
        }
        return dist;
    }

    private static Double deg2rad(Double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static Double rad2deg(Double rad) {
        return (rad * 180 / Math.PI);
    }
}
