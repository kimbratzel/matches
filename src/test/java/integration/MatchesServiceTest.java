package integration;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.core.type.TypeReference;
import com.swipejobs.models.Job;
import com.swipejobs.models.Worker;
import com.swipejobs.services.DataService;
import com.swipejobs.services.DataServiceImpl;
import com.swipejobs.services.MatchesService;
import com.swipejobs.services.MatchesServiceImpl;

public class MatchesServiceTest {

    private MatchesService matchesService = new MatchesServiceImpl();
    
    private DataService dataService = new DataServiceImpl();

    private static final int LIMIT = 3;
    private List<Worker> workers;
    private List<Job> jobs;

    
    @Before
    public void fetchData() {
        ClassLoader classLoader = getClass().getClassLoader();
        workers = dataService.fetchData(new File(classLoader.getResource("data/workers.json").getFile()), new TypeReference<List<Worker>>(){});
        jobs = dataService.fetchData(new File(classLoader.getResource("data/jobs.json").getFile()), new TypeReference<List<Job>>(){});
    }


    @Test
    public void numberOfMatchesShouldBeWithinCorrectRange() {
        
        workers.forEach(w -> {
            List<Job> matches = matchesService.findMatchesForWorker(w, jobs, LIMIT);
            
            //Assert number of matches is within correct range (0-3 inclusive)
            assertThat(matches.size(), is(greaterThanOrEqualTo(0)));
            assertThat(matches.size(), is(lessThanOrEqualTo(LIMIT)));
        });
    }
}
