package com.swipejobs.unit;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.swipejobs.models.Job;
import com.swipejobs.models.Job.Location;
import com.swipejobs.models.Worker;
import com.swipejobs.models.Worker.Availability;
import com.swipejobs.models.Worker.JobSearchAddress;
import com.swipejobs.services.MatchesService;
import com.swipejobs.services.MatchesServiceImpl;

public class MatchesServiceTest {

    private static final int LIMIT = 3;
    private MatchesService matchesService = new MatchesServiceImpl();

    private Worker worker;
    private Job job;
    private List<Job> jobs;

    @Before
    public void setup() {
        //Init a basic worker and job for each test 
        worker = getBasicWorker();
        job = getBasicJob();
        jobs = new ArrayList<Job>(Arrays.asList(job));
    }

    @Test
    public void shouldMatchJob() {
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldNotMatchWhenNoWorkersRequired() {
        job.setWorkersRequired(0);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(0, results.size());
        assertTrue(!results.contains(job));
    }
    @Test
    public void shouldMatchDriversLicenseWhenNotRequired() {
        job.setDriverLicenseRequired(false);
        worker.setHasDriversLicense(false);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldMatchDriversLicenseWhenRequired() {
        job.setDriverLicenseRequired(true);
        worker.setHasDriversLicense(true);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldNotMatchDriversLicense() {
        job.setDriverLicenseRequired(true);
        worker.setHasDriversLicense(false);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(0, results.size());
        assertTrue(!results.contains(job));
    }
    @Test
    public void shouldNotMatchCertificates() {
        String certificate = "some certificate";
        job.setRequiredCertificates(new ArrayList<String>(Arrays.asList(certificate)));
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(0, results.size());
        assertTrue(!results.contains(job));
    }
    @Test
    public void shouldMatchCertificates() {
        String certificate = "some certificate";
        job.setRequiredCertificates(new ArrayList<String>(Arrays.asList(certificate)));
        worker.setCertificates(new ArrayList<String>(Arrays.asList(certificate)));
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldMatchSkills() {
        String jobTitle = "some job title";
        job.setJobTitle(jobTitle);
        worker.setSkills(new ArrayList<String>(Arrays.asList(jobTitle)));
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldMatchDistance() {
        Location wynard = new Location(-33.8657423, 151.2039938);
        Location newtown = new Location(-33.8978205, 151.1766812);
        JobSearchAddress jobsearchAddress = new JobSearchAddress("km", 10, newtown.getLongitude(), newtown.getLatitude());
        
        job.setLocation(wynard);
        worker.setJobSearchAddress(jobsearchAddress);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(1, results.size());
        assertTrue(results.contains(job));
    }
    @Test
    public void shouldNotMatchDistance() {
        Location wynard = new Location(-33.8657423, 151.2039938);
        Location hobart = new Location(-42.8823388, 147.311042);
        JobSearchAddress jobsearchAddress = new JobSearchAddress("km", 10, hobart.getLongitude(), hobart.getLatitude());
        
        job.setLocation(wynard);
        worker.setJobSearchAddress(jobsearchAddress);
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(0, results.size());
        assertTrue(!results.contains(job));
    }
    @Test
    public void shouldNotMatchAvailability() {
        Availability a = new Availability("Tuesday", 2);
        worker.setAvailability(new ArrayList<Availability>(Arrays.asList(a)));
        
        List<Job> results = matchesService.findMatchesForWorker(worker, jobs, LIMIT);
        
        assertEquals(0, results.size());
        assertTrue(!results.contains(job));
    }


    /**
     * Returns a job with default basic parameters that matches with the worker below.
     */
    protected Job getBasicJob() {
        Job job = new Job()
                .setRequiredCertificates(new ArrayList<String>())
                .setDriverLicenseRequired(false)
                .setJobTitle("")
                .setLocation(new Location(0.0, 0.0))
                .setRequiredCertificates(new ArrayList<String>())
                .setStartDate(ZonedDateTime.parse("2016-10-10T09:29:19.188Z"))
                .setWorkersRequired(1);
        return job;
    }

    /**
     * Returns a worker with default basic parameters that matches with the job above.
     */
    protected Worker getBasicWorker() {
        Availability a = new Availability("Monday", 1);
        Worker worker = new Worker()
                .setAvailability(new ArrayList<Availability>(Arrays.asList(a)))
                .setCertificates(new ArrayList<String>())
                .setHasDriversLicense(false)
                .setJobSearchAddress(new JobSearchAddress("km", 0, 0.0, 0.0))
                .setSkills(new ArrayList<String>());
        return worker;
    }
}
