package com.swipejobs.unit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.swipejobs.helpers.DistanceHelper;

public class DistanceHelperTest {

    private static final String UNIT_KM = "km";

    @Test
    public void shouldReturnZero() {
        Double distance = DistanceHelper.distanceBetweenTwoPoints(0.0, 0.0, 0.0, 0.0, UNIT_KM);
        assertThat(distance, equalTo(0.0));
    }

    @Test
    public void shouldReturnCorrectDistance() {
        Double newtownLat = -33.8978205;
        Double newtownLong = 151.1766812;
        Double wynardLat = -33.8657423;
        Double wynardLong = 151.2039938;
        
        Double expectedDistanceKm = 4.5;
        Double errorMarginKm = 0.5;
        
        Double distance = DistanceHelper.distanceBetweenTwoPoints(newtownLat, newtownLong, wynardLat, wynardLong, UNIT_KM);
        assertThat(distance, is(closeTo(expectedDistanceKm, errorMarginKm)));
    }

    @Test
    public void shouldReturnCorrectDistanceRegardlessOfOrder() {
        Double newtownLat = -33.8978205;
        Double newtownLong = 151.1766812;
        Double wynardLat = -33.8657423;
        Double wynardLong = 151.2039938;
        
        Double expectedDistanceKm = 4.5;
        Double errorMarginKm = 0.5;
        
        Double distance = DistanceHelper.distanceBetweenTwoPoints(wynardLat, wynardLong, newtownLat, newtownLong, UNIT_KM);
        assertThat(distance, is(closeTo(expectedDistanceKm, errorMarginKm)));
    }
}