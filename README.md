Matches API
==============
##### Job/Worker Matching Demo
##### Kim Bratzel, 2016
--------------

Dependencies
--------------
 - Java 1.8
 - Maven

Build
--------------
Simply use Maven to build the project: 
 - ```mvn clean install``` 
or
 - ```mvn clean install -Dmaven.test.skip=true```


Running The Program
--------------
 - ```java -jar ./target/Matches-0.0.1-SNAPSHOT.jar``` and navigate to
 - ```http://localhost:8080/api/matches?workerId=N```
 - where ```N``` is the userId of the worker to match jobs to. For instance,
 - ```http://localhost:8080/api/matches?workerId=1```
